

# infra
## usage
```
ansible-playbook infra.yml --extra-vars "ansible_sudo_pass=XXXXXXXXXXXX" -e "user=<:user:>"
```

# deployment
## usage
```
# production
ansible-playbook deploy.yml -e "namespace=default" -e "hostname=dolibarr.hyoo.fr"
# staging
ansible-playbook deploy.yml -e "namespace=staging" -e "hostname=dolibarr-staging.hyoo.fr"
```


# Restore
In order to restore the BDD of Dolibarr

```
ansible-playbook restore.yml -e "namespace=target_namespace" -e "dolibarr_dump_mysql=name_of_the_mysql_dump"
```


# Upgrade

